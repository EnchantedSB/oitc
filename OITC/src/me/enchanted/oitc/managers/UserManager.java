package me.enchanted.oitc.managers;

import java.util.ArrayList;

import org.bukkit.entity.Player;

public class UserManager {
	
	private ArrayList<User> users = new ArrayList<User>();
	
	public User getUser(Player p) {
		for(User user : users) {
			if(user.getUniqueId().equals(p.getUniqueId())) {
				return user;
			}
		}
		return null;
	}
	
	public void addUser(Player p, boolean ingame, boolean alive, Arena game) {
		users.add(new User(p, ingame, alive, game));
	}
	
	public void removeUser(Player p) {
		users.remove(getUser(p));
	}
}
