package me.enchanted.oitc.managers;

import java.util.UUID;

import org.bukkit.entity.Player;

public class User {

	private final Player player;
	private boolean ingame,alive;
	private Arena game;
	
	public User(Player p, boolean ingame, boolean alive, Arena game) {
		this.player = p;
		this.ingame = ingame;
		this.alive = alive;
		this.game = game;
	}
	
	// Getters
	
	public UUID getUniqueId() {
		return player.getUniqueId();
	}
	
	public boolean isInGame() {
		return this.ingame;
	}
	
	public boolean isAlive() {
		return this.alive;
	}
	
	public Arena getGame() {
		return this.game;
	}
	
	// Setters
	
	public void setInGame(boolean ingame, Arena game) {
		this.ingame = ingame;
		this.game = game;
	}
	
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
}
