package me.enchanted.oitc.managers;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public class Arena {

	private String name;
	private List<Location> spawn = new ArrayList<Location>();
	private Set<User> players = new HashSet<User>();
	private Random random = new Random();

	public Arena(String name, Location spawn) {
		this.name = name;
		this.spawn.add(spawn);
	}

	// Getters

	public String getName() {
		return this.name;
	}

	public Location getRandomSpawn() {
		return this.spawn.get(random.nextInt(spawn.size()));
	}

	public Set<User> getPlayers() {
		return this.players;
	}

	// Setters

	public void addSpawn(Location loc) {

	}
}
