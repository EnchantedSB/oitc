package me.enchanted.oitc;

import org.bukkit.plugin.java.JavaPlugin;

import me.enchanted.oitc.managers.UserManager;

public class OITC extends JavaPlugin {
	
	private static OITC instance;
	private UserManager userManager;
	
	public void onEnable() {
		instance = this;
		userManager = new UserManager();
		
		getServer().getPluginManager().registerEvents(new GameListener(), this);
	}
	
	public static OITC getInstance() {
		return instance;
	}
	
	public UserManager getUserManager() { 
		return this.userManager;
	}

}
